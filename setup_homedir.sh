#!/usr/bin/env bash

source ./bash_functions.sh

# ----------========== GLOBALS ==========----------

USER=$1
SOURCE_DIR="/etc/skel/"
TARGET_DIR="/tmp/homedir/${USER}/"


# ----------========== FUNCTIONS ==========----------
usage() {
  #echo -e "\nUsage: $(basename $0) {username}\n" 1>&2
  echo -e ""
  e_bold "Usage: $(basename $0) {username}" 1>&2
  echo -e ""
  exit 1
}

# ----------========== MAIN ==========----------

#if [ $UID -ne 0 ]; then
#  e_error "This script requires root privaleges"
#fi

if [ "${USER}" == "" ]; then
  usage
fi

# check if the user exists
/usr/bin/getent passwd $USER > /dev/null

if [ $? -eq 0 ]; then
  # check that homedir doesn't already exist
  if [ -d $TARGET_DIR ]; then
    echo "${USER} already has a home directory here: ${TARGET_DIR}"
    exit 2
  else
    echo ""
    e_header "Setting up ${TARGET_DIR} for ${USER}"
    /usr/bin/rsync -a $SOURCE_DIR $TARGET_DIR &> /dev/null
    /bin/chmod 700 $TARGET_DIR &> /dev/null
    /bin/chown -R $USER $TARGET_DIR &> /dev/null
    e_success "Done!"
    echo ""
  fi
else
  echo ""
  e_error "User ${USER} doesn't exist."
  usage
fi

exit 0
